package tests;

import io.github.cdimascio.dotenv.Dotenv;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import pages.ResultInsuranceTravelPage;
import pages.SearchInsuranceTravelPage;

public class TestBase {

    protected WebDriver driver;
    protected SearchInsuranceTravelPage searchInsuranceTravelPage;
    protected ResultInsuranceTravelPage resultInsuranceTravelPage;

    public WebDriver getDriver() {
        return this.driver;
    }

    @BeforeMethod
    @Parameters("browser")
    public void testSetup(@Optional("chrome") String browser) {

        Dotenv dotenv = Dotenv.configure().directory("./").load();
        String URL = dotenv.get("URL");
        String HEADLESS = dotenv.get("HEADLESS");

        String CHROMEDRIVERDIR = "";
        if (System.getProperty("os.name").toLowerCase().contains("linux")) {
            CHROMEDRIVERDIR = dotenv.get("CHROMEDRIVERDIRLINUX");
        } else
            CHROMEDRIVERDIR = dotenv.get("CHROMEDRIVERDIRWIN");

        System.setProperty("webdriver.chrome.driver", CHROMEDRIVERDIR);


        if (browser.equalsIgnoreCase("firefox")) {
            FirefoxOptions options = new FirefoxOptions();
            options.setHeadless(Boolean.parseBoolean(HEADLESS));
            driver = new FirefoxDriver(options);
        } else {
            ChromeOptions options = new ChromeOptions();
            options.setHeadless(Boolean.parseBoolean(HEADLESS));
            options.addArguments("--disable-notifications");
            driver = new ChromeDriver(options);
        }

        driver.manage().window().maximize();
        driver.get(URL);
        searchInsuranceTravelPage = new SearchInsuranceTravelPage(driver);
    }

    @AfterMethod
    public void teardown() {
        driver.quit();
    }
}
