package tests;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utils.ExtentReports.ExtentTestManager;
import utils.Listeners.TestListener;

import java.lang.reflect.Method;
import java.util.List;

@Listeners({TestListener.class})

public class SearchResultTest extends TestBase {

    private int numberOfDays = 20;

    //Case 1: Verify if user can search successfully and direct to ResultInsuranceTravelPage
    @Test(description = "Verify if user can search successfully and direct to ResultInsuranceTravelPage")
    public void verifyResultPage(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if user can search successfully and direct to ResultInsuranceTravelPage");
        boolean isResultPage = searchInsuranceTravelPage.selectInsuranceTravelTab().showResult().verifyResultPage();
        Assert.assertTrue(isResultPage, "This is not a Search Result Page!");
    }

    //Case 2: Verify search keywords appear in Result page
    @Test(description = "Verify if search keywords appear in ResultInsuranceTravelPage")
    public void verifySearchKeywordsResultPage(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if search keywords appear in ResultInsuranceTravelPage");
        List<String> selectedComponentsValue = searchInsuranceTravelPage.selectInsuranceTravelTab().getSelectedComponentsValue();
        boolean isKeywordAppeared = searchInsuranceTravelPage.showResult().verifySearchKeywordResult(selectedComponentsValue);
        Assert.assertTrue(isKeywordAppeared, "Search keywords don't match!");
    }

    //Case 3: Verify number of results will match with the actual number of cards in Result page and bigger than 3
    @Test(description = "Verify if number of results will match with the actual number of cards in Result page and bigger than 3")
    public void verifyNumberOfCardsResultPage(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if number of results will match with the actual number of cards in Result page and bigger than 3");
        boolean isNumberOfCardsMatched = searchInsuranceTravelPage.selectInsuranceTravelTab().showResult().verifyNumberOfCards();
        Assert.assertTrue(isNumberOfCardsMatched, "The number of cards doesn't match with the search result!");
    }

    //Case 4: Verify sorted price Low to High by radio button in Result page
    @Test(description = "Verify if sort price Low to High by radio button in Result page")
    public void verifySortPriceLowHighRadio(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if sort price Low to High by radio button in Result page");
        boolean isSortedLowHigh = searchInsuranceTravelPage.selectInsuranceTravelTab().showResult().verifySortPriceLowHighRadio();
        Assert.assertTrue(isSortedLowHigh, "The price was not sorted Low to High!");
    }

    //Case 5: Verify new Destination by using Dropdown list in Result page
    @Test(description = "Verify if new Destination appears by using Dropdown list in Result page")
    public void verifyDestinationDropdown(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if new Destination appears by using Dropdown list in Result page");
        boolean isNewDestination = searchInsuranceTravelPage.selectInsuranceTravelTab().showResult().verifyDestinationDropdown();
        Assert.assertTrue(isNewDestination, "The new destination was not selected!");
    }

    //Case 6: Verify Filter by checkbox and Personal Accident slider in Result page
    @Test(description = "Verify if cards can be filtered by checkbox and Personal Accident slider in Result page")
    public void verifyFilteredByCheckboxRange(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if cards can be filtered by checkbox and Personal Accident slider in Result page");
        boolean isNewDestination = searchInsuranceTravelPage.selectInsuranceTravelTab().showResult().verifyCheckboxRangeFilter();
        Assert.assertTrue(isNewDestination, "The slider filter was not applied correctly!");
    }

    //Case 7: Verify Filter by Travel Start Date and End Date in Result page
    @Test(description = "Verify if cards can be filtered by Travel Start Date and End Date in Result page")
    public void verifyFilteredByDatePicker(Method method) {
        ExtentTestManager.startTest(method.getName(), "Verify if cards can be filtered by Travel Start Date and End Date in Result page");
        boolean isNewDate = searchInsuranceTravelPage.selectInsuranceTravelTab().showResult().verifyDatePickerFilter(numberOfDays);
        Assert.assertTrue(isNewDate, "The date filter was not applied correctly!");
    }

}
