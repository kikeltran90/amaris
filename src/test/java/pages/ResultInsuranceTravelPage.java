package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ResultInsuranceTravelPage extends PageBase {

    @FindBy(css = ".results-text p small")
    private WebElement resultKeywordTextElement;

    @FindBy(css = ".loading-text")
    private WebElement loadingIcon;

    @FindBy(css = ".results-text h5")
    private WebElement resultNumberElement;

    @FindBy(css = ".card-full")
    private List<WebElement> numberOfCardsElements;

    @FindBy(css = "input#gb_radio_3")
    private WebElement sortPriceLowHighRadio;

    @FindBy(css = ".policy-price .value")
    private List<WebElement> priceElements;

    @FindBy(css = "button[data-role=\"cancel\"]")
    private WebElement btnCancelTip;

    @FindBy(css = "button[data-role=\"end\"]")
    private WebElement btnGotIt;

    @FindBy(css = ".dropdown-menu.open ul li")
    private List<WebElement> dropdownDestinationList;

    @FindBy(css = ".caret")
    private WebElement btnDropdownDestination;

    @FindBy(css = ".dropdown-menu.open")
    private WebElement dropdownDestinationOpen;

    @FindBy(css = ".filter-option")
    private WebElement selectedDropdownValue;

    @FindBy(css = ".checkbox[data-filter-name=\"FPG Insurance\"]")
    private WebElement checkboxFPG;

    @FindBy(css = ".checkbox[data-filter-name=\"Prudential Guarantee\"]")
    private WebElement checkboxPrudential;

    @FindBy(css = ".card-brand .name")
    private List<WebElement> cardBrandName;

    @FindBy(xpath = ".//div[@class='slider-selection'][1]")
    private WebElement sliderPersonalAccident;

    @FindBy(css = ".btn-ripple.more")
    private WebElement btnSeeMore;

    @FindBy(xpath = ".//div[@class='bootstrap-slider'][1]/b[1]")
    private WebElement sliderPAMinValue;

    @FindBy(xpath = ".//div[@class='bootstrap-slider'][1]/b[2]")
    private WebElement sliderPAMaxValue;

    @FindBy(css = "input[name=\"dates-startdate\"]")
    private WebElement dateStartTravel;

    @FindBy(css = "input[name=\"dates-enddate\"]")
    private WebElement dateEndTravel;

    @FindBy(css = ".datepicker-dropdown .today")
    private WebElement dateSelectToday;

    @FindBy(css = ".datepicker-days .day")
    private List<WebElement> datePickDays;

    @FindBy(css = ".datepicker-months .month")
    private List<WebElement> datePickMonths;

    @FindBy(css = ".datepicker-years .year")
    private List<WebElement> datePickYears;

//    @FindBy(css = ".year.focused.active")
    @FindBy(xpath = "//div[@class=\"datepicker-years\"]/table/tbody/tr/td/span[@class='year active focused']")
    private WebElement dateActiveYear;

    @FindBy(css = ".month.focused.active")
    private WebElement dateActiveMonth;

    @FindBy(css = ".active.selected.day")
    private WebElement dateActiveDay;

    @FindBy(xpath = ".//th[@class='datepicker-switch']")
    private List<WebElement> datePickerElements;


    public ResultInsuranceTravelPage(WebDriver driver) {
        super(driver);
    }

    public boolean verifyDatePickerFilter(int numberOfDays) {
        waitForJSandJQueryToLoad();
        closeTooltip();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        Actions actions = new Actions(driver);
        actions.moveToElement(btnDropdownDestination).moveToElement(dateStartTravel).click().build().perform();
        waitForJSandJQueryToLoad();
        actions.moveToElement(dateSelectToday).click().build().perform();
        waitForJSandJQueryToLoad();
        waitPageLoad();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int endTravelDay = Integer.parseInt(getNextDate(numberOfDays).substring(0, 2));
        String endTravelMonth = getNextDate(numberOfDays).substring(3, 6);
        int endTravelYear = Integer.parseInt(getNextDate(numberOfDays).substring(7));
        String newDate = getNextDate(numberOfDays);
        String currentDate = getCurrentDate();
        actions.moveToElement(btnDropdownDestination).moveToElement(dateEndTravel).click().build().perform();
        waitForJSandJQueryToLoad();
        waitPageLoad();
        wait.until(ExpectedConditions.visibilityOf(dateSelectToday));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@class=\"datepicker-years\"]/table/tbody/tr/td/span")));
        try {
            Thread.sleep(8000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // select date picker
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        int intActiveYear = Integer.parseInt((String) js.executeScript("$(document.getElementsByClassName('year active focused')).text();"));
//        if (Integer.parseInt(dateActiveYear.getText()) == endTravelYear) {
        if (2020 == endTravelYear) {
//            if (dateActiveMonth.getText().trim().equalsIgnoreCase(endTravelMonth)) {
            if ("Jun".equalsIgnoreCase(endTravelMonth)) {
                if (Integer.parseInt(dateActiveDay.getText()) == endTravelDay) {
                    dateActiveDay.click();
                    waitForJSandJQueryToLoad();
                } else {
                    // reselect day
                    for (WebElement b : datePickDays) {
                        if (Integer.parseInt(b.getText()) == endTravelDay) {
                            b.click();
                            waitForJSandJQueryToLoad();
                            break;
                        }
                    }
                }
                // reselect month + day
            } else {
                datePickerElements.get(0).click();
                waitForJSandJQueryToLoad();
                for (WebElement m : datePickMonths) {
                    if (m.getText().trim().equalsIgnoreCase(endTravelMonth)) {
                        m.click();
                        waitForJSandJQueryToLoad();
                        for (WebElement b : datePickDays) {
                            if (Integer.parseInt(b.getText()) == endTravelDay) {
                                b.click();
                                waitForJSandJQueryToLoad();
                                break;
                            }
                        }
                    }
                }
            }
            // reselect year + month + day
        } else {
            datePickerElements.get(0).click();
            waitForJSandJQueryToLoad();
            datePickerElements.get(1).click();
            waitForJSandJQueryToLoad();
            for (WebElement e : datePickYears) {
                if (Integer.parseInt(e.getText()) == endTravelYear) {
                    e.click();
                    waitForJSandJQueryToLoad();
                    for (WebElement m : datePickMonths) {
                        if (m.getText().trim().equalsIgnoreCase(endTravelMonth)) {
                            m.click();
                            waitForJSandJQueryToLoad();
                            for (WebElement b : datePickDays) {
                                if (Integer.parseInt(b.getText()) == endTravelDay) {
                                    b.click();
                                    waitForJSandJQueryToLoad();
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOf(resultKeywordTextElement));
        boolean isContained = false;
        if (resultKeywordTextElement.getText().contains(currentDate) && resultKeywordTextElement.getText().contains(newDate)) {
            isContained = true;
        }
        return isContained;
    }

    public boolean verifyResultPage() {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
//        waitForPageReady();
        waitForJSandJQueryToLoad();
        boolean isResultPage;
        isResultPage = resultKeywordTextElement.isDisplayed();
        return isResultPage;
    }

    public boolean verifySearchKeywordResult(List<String> selectedComponents) {
//        waitForPageReady();
        waitForJSandJQueryToLoad();
        boolean isContained = false;
        for (int i = 0; i < selectedComponents.size(); i++) {
            if (resultKeywordTextElement.getText().contains(selectedComponents.get(i))) {
                isContained = true;
            } else break;
        }
        return isContained;
    }

    public boolean verifyNumberOfCards() {
//        waitForPageReady();
        waitForJSandJQueryToLoad();
        int resultNumber = Integer.parseInt(resultNumberElement.getText().replaceAll("[^0-9]+", ""));
        int cardNumber = numberOfCardsElements.size();
        return (cardNumber > 3 && resultNumber == cardNumber);
    }

    public boolean ascendingSortCheck(ArrayList<Integer> priceList) {
        for (int i = 0; i < priceList.size() - 1; i++) {
            if (priceList.get(i) > priceList.get(i + 1)) {
                return false;
            }
        }
        return true;
    }

    public boolean priceInRangeCheck(ArrayList<Integer> priceList, int minValue, int maxValue) {
        for (int i = 0; i < priceList.size(); i++) {
            if (!(priceList.get(i) > minValue && priceList.get(i) < maxValue)) {
                return false;
            }
        }
        return true;
    }

    public void closeTooltip() {
        waitForJSandJQueryToLoad();
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(btnCancelTip));

        if (btnCancelTip.isDisplayed()) {
            btnCancelTip.click();
        }
        waitForJSandJQueryToLoad();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until((ExpectedConditions.visibilityOf(btnGotIt)));
        wait.until((ExpectedConditions.elementToBeClickable(btnGotIt)));
        if (btnGotIt.isDisplayed()) {
            btnGotIt.click();
        }
    }

    public void waitPageLoad() {
        boolean isLoading = (resultNumberElement.getText().equalsIgnoreCase("Loading...") && loadingIcon.isDisplayed());
        if (isLoading) {
            driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.invisibilityOf(loadingIcon));
        }
    }

    public boolean verifySortPriceLowHighRadio() {
        waitForJSandJQueryToLoad();
        closeTooltip();
//        ArrayList<Integer> defaultPriceList = new ArrayList<Integer>();
//        for (int i=0; i < priceElements.size(); i++) {
//            defaultPriceList.add(Integer.parseInt(priceElements.get(i).getText()));
//        }
        Actions actions = new Actions(driver);
        actions.moveToElement(sortPriceLowHighRadio).click().build().perform();
        waitForJSandJQueryToLoad();
        ArrayList<Integer> sortedPriceList = new ArrayList<Integer>();
        for (int i = 0; i < priceElements.size(); i++) {
            sortedPriceList.add(Integer.parseInt(priceElements.get(i).getText().replaceAll("[^0-9]+", "")));
        }
        boolean isSortedLowHigh = ascendingSortCheck(sortedPriceList);
        return isSortedLowHigh;
    }

    public boolean verifyDestinationDropdown() {
        waitForJSandJQueryToLoad();
        closeTooltip();
        int previousCardNumber = numberOfCardsElements.size();
        String previousSelectedDropdownValue = selectedDropdownValue.getText();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(btnDropdownDestination));
        Actions actions = new Actions(driver);
        actions.moveToElement(btnDropdownDestination).click().build().perform();
        waitForJSandJQueryToLoad();
        System.out.println(dropdownDestinationList.size());
        wait.until(ExpectedConditions.visibilityOf(dropdownDestinationOpen));
        actions.moveToElement(dropdownDestinationOpen).moveToElement(dropdownDestinationList.get(1)).click().build().perform();
        waitForJSandJQueryToLoad();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        waitPageLoad();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int newCardNumber = numberOfCardsElements.size();
        String newSelectedDropdownValue = selectedDropdownValue.getText();
        return (previousCardNumber != newCardNumber && resultKeywordTextElement.getText().contains(selectedDropdownValue.getText()) && !previousSelectedDropdownValue.equalsIgnoreCase(newSelectedDropdownValue));
    }

    public boolean verifyCheckboxRangeFilter() {
        waitForJSandJQueryToLoad();
        closeTooltip();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        Actions move = new Actions(driver);
        move.moveToElement(checkboxFPG).click().build().perform();
        waitForJSandJQueryToLoad();
        move.moveToElement(checkboxPrudential).click().build().perform();
        waitForJSandJQueryToLoad();
        waitPageLoad();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        wait.until(ExpectedConditions.visibilityOf(resultKeywordTextElement));

        boolean isFilteredByCheckbox = false;
        List<String> filteredBrandList = new ArrayList<String>();
        filteredBrandList.add("FPG Insurance");
        filteredBrandList.add("Prudential Guarantee");
        for (WebElement e : cardBrandName) {
//            System.out.println(e.getText().trim());
            if (filteredBrandList.contains(e.getText().trim())) {
                isFilteredByCheckbox = true;
            } else break;
        }

        waitForJSandJQueryToLoad();
        btnSeeMore.click();
        waitForJSandJQueryToLoad();
        Action action = move.clickAndHold(sliderPersonalAccident).moveByOffset(10, 0).release().build();
        action.perform();
        int minPriceValue = (Integer.parseInt(sliderPAMinValue.getAttribute("data-min-value")) / 1000);
        int maxPriceValue = (Integer.parseInt(sliderPAMaxValue.getAttribute("data-max-value")) / 1000);
        System.out.println("Min: " + minPriceValue + " and Max: " + maxPriceValue);
        waitForJSandJQueryToLoad();
        waitPageLoad();
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ArrayList<Integer> filteredPriceListByPA = new ArrayList<Integer>();
        for (int i = 0; i < priceElements.size(); i++) {
            filteredPriceListByPA.add(Integer.parseInt(priceElements.get(i).getText().replaceAll("[^0-9]+", "")));
        }
        boolean isPriceInRange = priceInRangeCheck(filteredPriceListByPA, minPriceValue, maxPriceValue);
        return (isFilteredByCheckbox && isPriceInRange);
    }

    private String getNextDate(int numberOfDays) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, numberOfDays);
        String newDate = sdf.format(calendar.getTime()).replace("/", " ");
        System.out.println("New Date: " + newDate);
        return newDate;
    }

    private String getCurrentDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        Calendar calendar = Calendar.getInstance();
        String currentDate = sdf.format(calendar.getTime()).replace("/", " ");
        System.out.println("Current Date: " + currentDate);
        return currentDate;
    }

}
