package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SearchInsuranceTravelPage extends PageBase {

    @FindBy(css = "a[href=\"#Insurance\"]")
    private WebElement linkInsurance;

    @FindBy(css = "a[href=\"#Travel\"]")
    private WebElement linkTravel;

    @FindBy(css = "li[data-gb-name=\"Travel\"].active")
    private WebElement tabTravelActive;

    @FindBy(css = "#Travel .btn-form-submit")
    private WebElement btnFormSubmit;

    @FindBy(css = ".select-component")
    private List<WebElement> selectedComponents;


    public SearchInsuranceTravelPage(WebDriver driver) {
        super(driver);
    }

    public SearchInsuranceTravelPage selectInsuranceTravelTab() {
        waitForJSandJQueryToLoad();
        if (linkInsurance.isDisplayed()) {
            linkInsurance.click();
            waitForJSandJQueryToLoad();
            if (linkTravel.isDisplayed()) {
                linkTravel.click();
            }
        }
        waitForJSandJQueryToLoad();
        return this;
    }

    public List<String> getSelectedComponentsValue() {
        waitForJSandJQueryToLoad();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        List<String> selectedComponentsValues = new ArrayList<>();
        for (int i = 0; i < selectedComponents.size(); i++) {
            if (i == 1) {
                selectedComponentsValues.add(selectedComponents.get(i).getText().replace("just", "") + " ");
            } else {
                selectedComponentsValues.add(selectedComponents.get(i).getText());
            }
        }
        return selectedComponentsValues;
    }

    public ResultInsuranceTravelPage showResult() {
        waitForJSandJQueryToLoad();
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.visibilityOf(tabTravelActive));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#Travel .btn-form-submit")));
        wait.until(ExpectedConditions.elementToBeClickable(btnFormSubmit));
        if (selectedComponents.get(0).getText().contains("single trip")) {
            btnFormSubmit.click();
        }
        return new ResultInsuranceTravelPage(driver);
    }

}
