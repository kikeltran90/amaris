1. Setup [Java and Maven environments](https://www.tutorialspoint.com/maven/maven_environment_setup.htm )
2. Extract the zip file.
3. Configure chromedriver in file env (if needed)
4. Open terminal, run command `mvn clean install -DskipTests=true` at project folder to install dependencies
5. To run test, run command `mvn test -DsuiteXmlFile={file.xml}`
> Example: `mvn test -DsuiteXmlFile=testng.xml`